#!/usr/bin/env python
import os
import json
import argparse
from pprint import pprint
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.parse import urljoin, urlparse
from xml.dom.minidom import getDOMImplementation, Element
from inspect import cleandoc

import scrapy
from scrapy.crawler import CrawlerProcess


class Resource:
    """
    Web resource
    """
    def __init__(self, response):
        self.url = response.url
        self.response = response


class ThingSchema:
    """
    Microdata description as from schema.org
    """
    loaded_schemas = {}

    @staticmethod
    def _trimschema(name):
        if name.startswith('schema:'):
            return name[len('schema:'):]

    def __init__(self, name):
        url = "https://schema.org/%s.jsonld" % name
        schema = json.load(urlopen(url))
        self.name = name
        parent = self._trimschema(schema.get('rdfs:subClassOf', {}).get("@id", ''))
        self.parent = None if not parent or parent == name else self.get_schema(parent)
        self.properties = {
            self._trimschema(node['@id'])
            for node in schema.get("@graph", [])
            if node.get('@type', '') == 'rdf:Property'
        }

    def has_property(self, propname):
        """
        Whether a property is in the description
        """
        return propname in self.properties or (self.parent and self.parent.has_property(propname))

    @classmethod
    def get_schema(cls, name):
        """
        Returns the schema for the given type, caching results to avoid spamming schema.org
        """
        if name not in cls.loaded_schemas:
            try:
                cls.loaded_schemas[name] = cls(name)
            except HTTPError:
                return None
        return cls.loaded_schemas[name]

    @classmethod
    def validate(cls, thing):
        """
        Returns a dict of properties => errors for the given Thing
        """
        schema = cls.get_schema(thing.type)
        if not schema:
            return {thing.type: "Unknown thing"}

        errors = {}
        for prop, value in thing.props:
            if not schema.has_property(prop):
                cls._add_error(prop, 'Not a recognized property', errors)
            elif isinstance(value, Thing):
                sub_issues = cls.validate(value)
                if sub_issues:
                    cls._add_error(prop, sub_issues, errors)
        return errors

    @classmethod
    def _add_error(cls, prop, error, out):
        if prop not in out:
            out[prop] = [error]
        else:
            out[prop].append(error)


class Thing:
    """
    Microdata object
    """
    def __init__(self, type):
        self.props = []
        self.type = type

    def add_prop(self, name, value):
        """
        Sets a property (there can be multiple properties with the same value)
        """
        self.props.append((name, value))

    def __str__(self):
        return self.type

    def pprint(self, indent_str="  ", indent_level=0):
        """
        Pretty-prints the property
        """
        indent = indent_str * indent_level
        print("%stype: %s" % (indent, self.type))
        for name, value in self.props:
            if isinstance(value, Thing):
                print("%s%s:" % (indent, name))
                value.pprint(indent_str, indent_level+1)
            else:
                print("%s%s: %s" % (indent, name, value))
        if indent_level == 0:
            print("")

    def validate(self):
        """
        Validates based on the schema
        """
        return ThingSchema.validate(self)

    @property
    def schema(self):
        """
        Returns the schema object for this thing
        """
        return ThingSchema.get_schema(self.type)


class Page(Resource):
    """
    Metadata for a HTML page
    """
    def __init__(self, response):
        super().__init__(response)
        self.pagemeta = Thing("unknown")
        self._things = None
        self._parse_meta_tags(self.response.selector.xpath("//head/meta"))

    @property
    def things(self):
        """
        Lazily load things to avoid parsing the DOM multiple times
        """
        if self._things is None:
            self._things = []
            self._find_objects(self.response.selector.xpath("//body")[0].root)
        return self._things

    def _parse_meta_tags(self, selector):
        """
        Parses the <head> metadata
        """
        for meta in selector:
            try:
                if "name" in meta.attrib:
                    key = "name"
                else:
                    key = "property"
                propname = meta.attrib[key]
                if propname == "viewport":
                    continue
                elif propname.startswith("og:"):
                    propname = propname[3:]

                propvalue = meta.attrib["content"]
                if propname == "type":
                    self.pagemeta.type = propvalue
                else:
                    self.pagemeta.add_prop(propname, propvalue)
            except KeyError:
                pass

    def _find_objects(self, dom_node):
        """
        Finds all Things in dom_node
        """
        if "typeof" in dom_node.attrib:
            objmeta = self._parse_object_meta(dom_node)
            self._things.append(objmeta)
            return

        for node in dom_node.iterchildren():
            self._find_objects(node)

    def _parse_object_meta(self, dom_node):
        """
        Given that dom_node is a Thing, it parses it and returns the associated Thing
        """
        objmeta = Thing(dom_node.attrib["typeof"])
        self._get_object_props(dom_node, objmeta)
        return objmeta

    def _get_object_props(self, dom_node, out):
        """
        Find properties in a Thing child, recurively
        """
        for c in dom_node.iterchildren():
            if "property" in c.attrib:
                out.add_prop(c.attrib["property"], self._extract_prop(c))
            elif "typeof" in c.attrib:
                self._parse_object_meta(c)
            else:
                self._get_object_props(c, out)

    def _extract_prop(self, dom_node):
        """
        Given that dom_node describe a Thing property, return the property value
        """
        if "typeof" in dom_node.attrib:
            return self._parse_object_meta(dom_node)

        if dom_node.tag == "meta":
            return dom_node.attrib.get("content", "")
        elif dom_node.tag == "img":
            return dom_node.attrib.get("src", "")
        elif dom_node.tag == "a":
            self._find_objects(dom_node)
            return dom_node.attrib.get("href", "")

        # find nested objects
        self._find_objects(dom_node)
        return str(dom_node.text).strip()


class MultilineLogger:
    """
    Logger for verbose output that doesn't spam the terminal window.
    Use as a context manager
    """
    def __init__(self):
        self._last_lines = 0
        self._curr_lines = 0

    def __enter__(self):
        """
        Set ups a new output chunck, each chunk will overwrite existing output
        Returns a function to be used for logging
        """
        self._curr_lines = 0
        return self._log

    def __exit__(self, *a):
        while self._curr_lines < self._last_lines:
            self._log("")
        self._last_lines = self._curr_lines

    def _log(self, line):
        """
        Actual logging function, produces ANSI codes to clear old output
        TODO handle newline characters in line
        """
        pre = "\r\x1b[2K"
        if self._curr_lines == 0 and self._last_lines != 0:
            pre = "\x1b[%sA%s" % (self._last_lines, pre)
        print("%s%s" % (pre, line))
        self._curr_lines += 1


class Spiderg(scrapy.Spider):
    """
    Crawlier
    """
    name = "spiderg"
    start_urls = ['http://localhost:81/']

    def __init__(self, name=None, **kwargs):
        start_url = kwargs.pop("base_url", None)
        self.follow_links = True
        self.follow_src = False
        self.quiet = False
        self.validate = False
        self.details = False
        self.report = None
        media = kwargs.pop("media", None)

        super().__init__(name, **kwargs)

        if start_url:
            self.start_urls = [start_url]
        self.follow_urls = list(self.start_urls)
        if media:
            self.follow_urls.append(media)

        if self.report:
            self.validate = True

        self.pages = []
        self.resources = []
        self.problem_count = 0
        self.mlogger = MultilineLogger()
        self.tally_res = {}
        self.tally_page = {}
        self.tally_microdata = {}

    def parse(self, response):
        """
        Called every time the crawler downloads a page
        """
        try:
            for r in self._parse(response):
                yield r
        except:
            # clear the logger state so the exception message isn't overwritten
            self.mlogger._last_lines = 0
            raise

    def _parse(self, response):
        if not self.quiet:
            with self.mlogger as log:
                log("Loading...")
                log("Current")
                log(response.url)
                log(response.status)
                self._log_stats(log)

        # Missing mime
        if "Content-Type" not in response.headers:
            page = Resource(response)
            self.problem_count += 1
            page.problems = ["No content type"]
        # Not HTML
        elif b"text/html" not in response.headers["Content-Type"]:
            page = Resource(response)
            page.problems = []
            mime = response.headers["Content-Type"].decode("ascii")
            if mime not in self.tally_res:
                self.tally_res[mime] = 1
            else:
                self.tally_res[mime] += 1
            self.resources.append(page)
        # HTML
        else:
            page = Page(response)
            page.problems = []
            type = page.pagemeta.type
            if type not in self.tally_page:
                self.tally_page[type] = 1
            else:
                self.tally_page[type] += 1
            self.pages.append(page)
            for req in self._follow(response):
                yield req

            if self.validate:
                errors = False
                if not page.things:
                    self.problem_count += 1
                    page.problems.append("Missing microdata")
                for thing in page.things:
                    if thing.type not in self.tally_microdata:
                        self.tally_microdata[thing.type] = 1
                    else:
                        self.tally_microdata[thing.type] += 1
                    if thing.validate():
                        errors = True

                if errors:
                    self.problem_count += 1
                    page.problems.append("Invalid microdata")

        if response.status != 200:
            page.problems.append("Status is %s" % response.status)

    def _follow(self, response):
        """
        Finds all the URIs to follow
        """
        if self.follow_links:
            for link in response.selector.xpath("//*[@href and not(contains(@rel, 'nofollow'))]/@href").extract():
                links_to = self._clean_follow(response, link)
                if links_to:
                    yield scrapy.Request(
                        links_to,
                        callback=self.parse
                    )
        if self.follow_src:
            for link in response.selector.xpath("//*[@src]/@src").extract():
                links_to = self._clean_follow(response, link)
                if links_to:
                    yield scrapy.Request(
                        links_to,
                        callback=self.parse
                    )

    def _clean_follow(self, response, links_to):
        """
        Makes sure a link to follow is cleaned up and ready for a new request
        """
        if links_to.endswith("?download"):
            links_to = links_to[:-9]

        if not links_to:
            return None

        links_to = response.urljoin(links_to)

        for base_url in self.follow_urls:
            if links_to.startswith(base_url):
                return links_to

        return None

    def closed(self, reason):
        """
        Called when the crawler finishes, produces reports
        """
        if not self.quiet:
            with self.mlogger as log:
                self._log_stats(log)

        if self.details and self.pages:
            page = self.pages[0]
            print("Metadata for %s" % page.url)
            page.pagemeta.pprint()
            print("Objects:")
            for thing in page.things:
                thing.pprint()
                if self.validate:
                    errors = thing.validate()
                    if errors:
                        print("Schema errors:")
                        pprint(errors)
                        print("")

        if self.report:
            ReportBuilder(self, self.report).build_report()

    def _log_stats(self, log):
        """
        Logs a summary of the currently loaded pages
        """
        log("Results")

        log("%d resources" % len(self.resources))
        self._log_tally(log, self.tally_res)

        log("%d pages" % len(self.pages))
        self._log_tally(log, self.tally_page)

        log("%d issues" % self.problem_count)
        if self.problem_count:
            for page in self.pages:
                if page.problems:
                    log("    %s" % page.url)
                    for problem in page.problems:
                        log("        %s" % problem)

        if self.validate:
            log("top-level microdata objects")
            self._log_tally(log, self.tally_microdata)

    def _log_tally(self, log, tallydict):
        """
        Display count/names from a tally dict
        """
        for n, c in tallydict.items():
            log("    %4d %s" % (c, n))


class SimplerThanDomNode:
    """
    Used to build XML/HTML files more concisely than using DOM directly
    """
    def __init__(self, domelement):
        self.dom = domelement

    def to_xml(self, pretty=False):
        return self.dom.toxml("utf-8") if not pretty else self.dom.toprettyxml(encoding="utf-8")


class SimplerThanDomAttrlist:
    """
    Dict-like access to DOM element attributes
    """
    def __init__(self, elem):
        self.elem = elem

    def __getitem__(self, key):
        return self.elem.getAttribute(key)

    def __setitem__(self, key, value):
        return self.elem.setAttribute(key, value)


class SimplerThanDomElement(SimplerThanDomNode):
    """
    Used to build XML/HTML elements more concisely than using DOM directly
    """
    def e(self, tag, **attrs):
        """
        Shorthand to append_element()
        Creates a child element in the current node and returns it
        """
        return self.append_element(tag, **attrs)

    def t(self, text):
        """
        Shorthand to append_text()
        Appends a text node to the element, returns self to allow chaining
        """
        self.append_text(text)
        return self

    def p(self):
        """
        Shorthand to self.parent
        """
        return self.parent

    def et(self, tag, text, **attrs):
        """
        Combines e() and t()
        Creates a new child element, adds a text node to it,
        and returns self for chaining
        """
        self.e(tag, **attrs).t(text)
        return self

    def append_element(self, tag, **attrs):
        """
        Creates a child element in the current node and returns it
        """
        child = SimplerThanDomElement(self.dom.ownerDocument.createElement(tag))
        self.dom.appendChild(child.dom)
        for k, v in attrs.items():
            child.dom.setAttribute(k, v)
        return child

    def append_text(self, text):
        """
        Appends a text node to the element, returns self to allow chaining
        """
        txt = self.dom.ownerDocument.createTextNode(text)
        self.dom.appendChild(txt)

    def add_sibling_after(self, tag, **attrs):
        """
        Creates an element and adds it as a sibling following the current node
        Returns the created element
        """
        child = SimplerThanDomElement(self.dom.ownerDocument.createElement(tag))
        self.dom.parentNode.insertBefore(child.dom, self.dom.nextSibling)
        for k, v in attrs.items():
            child.dom.setAttribute(k, v)
        return child

    def add_sibling_before(self, tag, **attrs):
        """
        Creates an element and adds it as a sibling preceding the current node
        Returns the created element
        """
        child = SimplerThanDomElement(self.dom.ownerDocument.createElement(tag))
        self.dom.parentNode.insertBefore(child.dom, self.dom)
        for k, v in attrs.items():
            child.dom.setAttribute(k, v)
        return child

    def remove(self):
        """
        Removes the element from its parent
        """
        self.dom.parentNode.removeChild(self.dom)

    @property
    def empty(self):
        """
        If True, the element doesn't have any child nodes
        """
        return not self.dom.hasChildNodes()

    @property
    def parent(self):
        """
        Returns the parent element or None if the parent node isn't an element
        (or there is no parent node)
        """
        parent = self.dom.parentNode
        if not parent or not isinstance(parent, Element):
            return None
        return SimplerThanDomElement(parent)

    @property
    def document(self):
        """
        Returns the owning document
        """
        return SimplerThanDomNode(self.dom.ownerDocument)

    @property
    def attrs(self):
        """
        Gives dict-like access to the attributes
        """
        return SimplerThanDomAttrlist(self.dom)

    @classmethod
    def new_document(self, root="html"):
        """
        Creates an empty document and returns the root element
        """
        return SimplerThanDomElement(
            getDOMImplementation()
            .createDocument(None, "html", None)
            .documentElement
        )


class ReportBuilder:
    """
    Builds a report on the crawr result
    """
    def __init__(self, spiderg, outdir):
        self.spiderg = spiderg
        self.outdir = outdir

    def build_report(self):
        """
        Builds and saves the report
        """
        self._css()
        index_html, index_body = self._mk_html("Summary")
        error_ul = index_body.e("ul")
        table = index_body.e("table")
        table.e("tr").et("th", "url").et("th", "issues").et("th", "microdata")

        for page in self.spiderg.pages:
            html = self._page_report(page)
            self._prepare_report(page, html, table, error_ul)

        for resource in self.spiderg.resources:
            html = self._resource_report(resource)
            self._prepare_report(resource, html, table, error_ul)

        if error_ul.empty:
            error_ul.remove()
        else:
            error_ul.add_sibling_before("p").t("Errors:")

        if (
            len(self.spiderg.pages) + len(self.spiderg.resources) > 1
            or not os.path.exists(os.path.join(self.outdir, "index.htm"))
        ):
            self._save_html(index_html, "index.htm")
            report_path = os.path.abspath(os.path.join(self.outdir, "index.htm"))
            print("Report saved at %s" % urljoin("file://", report_path))

    def _prepare_report(self, page, html, table, error_ul):
            """
            Saves a page report and adds some info on the index page
            """
            path = urlparse(page.url).path
            rel_url = "pages/" + urlparse(page.url).netloc + path.rstrip("/") + ".htm"
            self._save_html(html, rel_url)

            tr = table.e("tr", id="page_%s" % id(page))

            a = tr.e("td").e("a", href=rel_url).t(path)
            if page.problems:
                a.attrs["class"] = "error"
                error_ul.e("li").et("a", path, href=rel_url)

            ul = tr.e("td").e("ul")
            for problem in page.problems:
                ul.et("li", problem)

            ul = tr.e("td").e("ul")
            for thing in getattr(page, "things", []):
                ul.e("li").et("a", thing.type, href=rel_url+"#thing_%s" % id(thing))

    def _css(self):
        """
        Outputs the CSS file
        """
        full_path = os.path.join(self.outdir, "style.css")
        os.makedirs(os.path.dirname(full_path), exist_ok=True)
        with open(full_path, "w") as file:
            file.write(cleandoc("""
                th {
                    text-align: left;
                }
                th, td {
                    border: 1px solid silver;
                    padding: 1px 0;
                }
                table {
                    border-collapse: collapse;
                    width: 100%;
                }
                body > table {
                    margin-bottom: 1em;
                }
                table table {
                    margin: -1px;
                }
                .error {
                    color: red;
                }
                table:target {
                    background-color: #ffe;
                }
                """))

    def _save_html(self, html, path):
        """
        Saves an HTML element to the given (relative) path
        """
        full_path = os.path.join(self.outdir, path)
        dirname = os.path.dirname(full_path)
        os.makedirs(dirname, exist_ok=True)

        css_path = os.path.join(os.path.relpath(self.outdir, dirname), "style.css")
        SimplerThanDomElement(html.dom.getElementsByTagName("head")[0]).e(
            "link", rel="stylesheet", href=css_path, type="text/css"
        )

        with open(full_path, "wb") as file:
            file.write(b"<!DOCTYPE html>")
            file.write(html.to_xml(True))

    def _resource_report(self, resource):
        """
        Generates HTML for a resource
        """
        html, body = self._mk_html(resource.url)
        body.et("h2", "Info")
        table = body.append_element("table")
        table.e("tr").et("th", "Link").e("td").et("a", resource.url, href=resource.url)
        mime = resource.response.headers.get("Content-Type", b"(missing)").decode("ascii")
        table.e("tr").et("th", "Mime type").et("td", mime)
        self._print_resource_issues(resource, body)
        if "image" in mime:
            body.et("h2", "Preview")
            body.e("img", src=resource.url, alt="")
        return html

    def _mk_html(self, title):
        """
        Generates an empty HTML page
        """
        html = SimplerThanDomElement.new_document()
        head = html.e("head")
        head.et("title", title)
        head.e("meta", charset="utf-8")
        body = html.append_element("body")
        body.et("h1", title)
        return html, body

    def _print_resource_issues(self, resource, body):
        """
        Generates an issue summary
        """
        if resource.problems:
            body.et("h2", "Issues")
            ul = body.append_element("ul")
            for error in resource.problems:
                ul.et("li", error)

    def _page_report(self, page):
        """
        Generates HTML for a page
        """
        html, body = self._mk_html(page.url)
        body.et("h2", "Info")
        table = body.append_element("table")
        table.e("tr").et("th", "Link").e("td").et("a", page.url, href=page.url)

        body.et("h2", "Page Metadata")
        table = body.append_element("table")
        table.e("tr").et("th", "type").et("td", page.pagemeta.type)
        for p, v in page.pagemeta.props:
            table.e("tr").et("th", p).et("td", v)

        self._print_resource_issues(page, body)

        if page.things:
            h2 = body.e("h2").t("Microdata")
            errlinks = []
            for thing in page.things:
                self._show_thing(thing, body, errlinks)
            if errlinks:
                ul = h2.add_sibling_after("ul")
                for id, prop, type in errlinks:
                    ul.e("li").t("Property ").et("a", "%s.%s" % (type, prop), href="#"+id).t(" not found")
        return html

    def _show_thing(self, thing, parent, errlinks):
        """
        Recurively generate HTML for all the properties of the thing
        """
        table = parent.append_element("table", id="thing_%s" % id(thing))
        table.e("tr").et("th", "type").et("td", thing.type)
        for p, v in thing.props:
            tr = table.e("tr")
            th = tr.e("th")
            td = tr.e("td")
            th.t(p)
            if not thing.schema.has_property(p):
                th.attrs["class"] = "error"
                th.attrs["title"] = "Property not found"
                th.attrs["id"] = "error_%s" % len(errlinks)
                errlinks.append((th.attrs["id"], p, thing.type))
            if isinstance(v, Thing):
                self._show_thing(v, td, errlinks)
            else:
                td.t(v)


def parse_args(args=None):
    """
    Parse command line arguments
    """
    parser = argparse.ArgumentParser(
        description="Crawls a website and extracts/validates metadata"
    )
    parser.add_argument(
        "base_url",
        help="URL to parse",
    )
    parser.add_argument(
        "--media", "-m",
        default="/media",
        help="Media path"
    )
    parser.add_argument(
        "--src",
        action="store_true",
        default=False,
        dest="follow_src",
        help="Follow src attributes"
    )
    parser.add_argument(
        "--no-src",
        action="store_false",
        default=False,
        dest="follow_src",
        help="Don't follow src attributes (default)"
    )
    parser.add_argument(
        "--link", "-r", "-R",
        action="store_true",
        default=False,
        dest="follow_links",
        help="Follow <a> elements (default)"
    )
    parser.add_argument(
        "--no-link",
        action="store_false",
        default=False,
        dest="follow_links",
        help="Don't follow <a> elements"
    )
    parser.add_argument(
        "--quiet", "-q",
        action="store_true",
        default=False,
        help="Don't log process"
    )
    parser.add_argument(
        "--details", "-d",
        action="store_true",
        default=False,
        help="Show metadata details of the starting page"
    )
    parser.add_argument(
        "--validate", "-v",
        action="store_true",
        default=False,
        help="Validate metadata of the starting page"
    )
    parser.add_argument(
        "--report", "-o", "--html",
        nargs="?",
        default=None,
        help="Output an HTML report"
    )
    args = vars(parser.parse_args())
    args["base_url"] = urljoin("http:", args["base_url"])
    args["media"] = urljoin(args["base_url"], args["media"])
    return args


if __name__ == "__main__":
    cmd_args = parse_args()
    settings = {
        "LOG_LEVEL": "WARNING",
    }
    cp = CrawlerProcess(settings)
    cp.crawl(Spiderg, **cmd_args)
    cp.start()
